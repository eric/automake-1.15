Source: automake-1.15
Section: devel
Priority: optional
Maintainer: Eric Dorland <eric@debian.org>
Standards-Version: 4.2.1.1
Build-Depends: debhelper-compat (= 11)
Build-Depends-Indep: autoconf (>= 2.65),
                     autopoint,
                     bison,
                     cscope,
                     dejagnu,
                     default-jdk,
                     emacs,
                     expect,
                     flex,
                     gettext,
                     gfortran,
                     gobjc,
                     gobjc++,
                     help2man,
                     install-info,
                     libglib2.0-dev,
                     libtool,
                     libtool-bin,
                     lzip,
                     ncompress,
                     pkg-config,
                     python,
                     python-docutils,
                     python-virtualenv,
                     sharutils,
                     texinfo (>= 4.9),
                     texlive,
                     unzip,
                     valac,
                     xutils-dev,
                     zip
Homepage: https://www.gnu.org/software/automake/
Vcs-Git: https://salsa.debian.org/eric/automake-1.15.git
Vcs-Browser: https://salsa.debian.org/eric/automake-1.15

Package: automake-1.15
Architecture: all
Multi-Arch: foreign
Provides: automaken
Breaks: automake (<= 1:1.16-1~)
Replaces: automake (<= 1:1.16-1~)
Depends: autoconf (>= 2.65), autotools-dev (>= 20020320.1), ${misc:Depends}
Suggests: autoconf-doc, gnu-standards
Description: Tool for generating GNU Standards-compliant Makefiles
 Automake is a tool for automatically generating `Makefile.in's from
 files called `Makefile.am'.
 .
 The goal of Automake is to remove the burden of Makefile maintenance
 from the back of the individual GNU maintainer (and put it on the back
 of the Automake maintainer).
 .
 The `Makefile.am' is basically a series of `make' macro definitions
 (with rules being thrown in occasionally).  The generated
 `Makefile.in's are compliant with the GNU Makefile standards.
 .
 Automake 1.15 fails to work in a number of situations that Automake
 1.4, 1.6, 1.7, 1.8, 1.9, 1.10, 1.11 and 1.14 did, so previous versions are
 available as separate packages.
